/**
 * Riff Scrobbler Chrome Extension 
 */
console.log ("Riff Scrobbler Chrome Extension Enabled");

var supportedPlayers=new Array("rdio","spotify","tracks","deezer","iheart","myspace","pandora","soundcloud","grooveshark","songza", "vk", "google", "gaana", "youtube"); 

// The first time they open the popup, show a notification telling them what to do.
if (!localStorage["pastFirstTime"]) {
    var setupNotificationId = "19";
    chrome.notifications.create(setupNotificationId, {
        type: "basic",
        iconUrl: "icon128.png",
        title: "Riff Music Capture",
        message: "You need to enter an access code that you get from your Riff app.",
        priority: 2,
        buttons: [{ title: "Cool" }],
        isClickable: false}, 
        function(notificationId) {}
    );

    localStorage["pastFirstTime"] = true;
}

var iconNot;
var notification;
var message;

/**
 * Turn on or off player switches.
 */
document.addEventListener('DOMContentLoaded', restore_options_s);

/**
 * Determine login status
 */
if  (localStorage["session"] === "" || !localStorage["session"]) 
    swap("whenLoggedOut", "whenLoggedIn");
else
    swap("whenLoggedIn", "whenLoggedOut");

document.addEventListener('DOMContentLoaded', function () { document.getElementById("rdio").addEventListener('change', function(){ changeImage("rdio"); },false) });   
document.addEventListener('DOMContentLoaded', function () { document.getElementById("spotify").addEventListener('click', function(){ changeImage("spotify"); },false) });   
document.addEventListener('DOMContentLoaded', function () { document.getElementById("tracks").addEventListener('click', function(){ changeImage("tracks"); },false) });   
document.addEventListener('DOMContentLoaded', function () { document.getElementById("deezer").addEventListener('click', function(){ changeImage("deezer"); },false) });   
document.addEventListener('DOMContentLoaded', function () { document.getElementById("myspace").addEventListener('click', function(){ changeImage("myspace"); },false) });   
document.addEventListener('DOMContentLoaded', function () { document.getElementById("pandora").addEventListener('click', function(){ changeImage("pandora"); },false) });   
document.addEventListener('DOMContentLoaded', function () { document.getElementById("grooveshark").addEventListener('click', function(){ changeImage("grooveshark"); },false) });   
document.addEventListener('DOMContentLoaded', function () { document.getElementById("songza").addEventListener('click', function(){ changeImage("songza"); },false) });   
document.addEventListener('DOMContentLoaded', function () { document.getElementById("vk").addEventListener('click', function(){ changeImage("vk"); },false) }); 
document.addEventListener('DOMContentLoaded', function () { document.getElementById("google").addEventListener('click', function(){ changeImage("google"); },false) }); 
document.addEventListener('DOMContentLoaded', function () { document.getElementById("gaana").addEventListener('click', function(){ changeImage("gaana"); },false) });
document.addEventListener('DOMContentLoaded', function () { document.getElementById("youtube").addEventListener('click', function(){ changeImage("youtube"); },false) });

document.addEventListener('DOMContentLoaded', function () { populateSongMeta(); });
document.addEventListener('DOMContentLoaded', function () { document.getElementById("riff-now").addEventListener('click', function(e) { startRiffOnDevices(e); })});

function populateSongMeta() {
  var scrobble = localStorage["lastRiffScrobble"];
  if (scrobble) {
    scrobble = JSON.parse(scrobble);
    document.getElementById("song-meta").innerHTML = scrobble.track;
  }
}

function startRiffOnDevices(e) {
  //var scrobble = localStorage["lastRiffScrobble"];
  //if (scrobble) {
  //  scrobble = JSON.parse(scrobble);
  //  document.getElementById("song-meta").innerHTML = scrobble.track;
  //}
  //

//  pushToRiff('push');
  chrome.runtime.sendMessage({ type: 'riffImmediatePushCurrent' });

  e.preventDefault();
  e.stopPropagation();
}

var connectors = [
                                    {
                                            label: "myspace",
                                            description : "MySpace"
                                    },
                                    {
                                            label: "pandora",
                                            description : "Pandora Internet Radio"
                                    },
                                    {
                                            label: "deezer",
                                            description : "Deezer"
                                    },
                                    {
                                            label: "google",
                                            description : "Google Play Music"
                                    },
                                    {
                                            label: "myspace",
                                            description : "MySpace"
                                    },
                                    {
                                            label: "soundcloud",
                                            description : "Soundcloud"
                                    },
                                    {
                                            label: "vk",
                                            description : "VK"
                                    },

                                    {
                                            label: "spotify",
                                            description : "Spotify Web Player"
                                    },
                                    {
                                            label: "grooveshark",
                                            description : "Gooveshark"
                                    },
                                    {
                                            label: "tracks",
                                            description : "8-Tracks"
                                    },
                                    {
                                            label: "songza",
                                            description : "Songza"
                                    },
                                    {
                                            label: "rdio",
                                            description : "Rdio"
                                    },
                                    {
                                            label: "gaana",
                                            description : "Gaana"
                                    },
                                    {
                                        label: "youtube",
                                        description: "YouTube"
                                    }
                            ];
/**
 * Connect/Remove event handlers.
 */
document.addEventListener('DOMContentLoaded', function () { document.getElementById("connect").addEventListener('click', function(){ connectToRiff(); },false) });  
document.addEventListener('DOMContentLoaded', function () { document.getElementById("lastname").addEventListener('keyup', function(e){ if(e.keyCode === 13 ){ e.preventDefault(); connectToRiff();  } },false) });
/**
 * Need to suppress the default form behaviour for enter. 
 */
document.addEventListener('DOMContentLoaded', function () { document.getElementById("lastname").addEventListener('keypress', function(e){ if(e.keyCode === 13 ){ e.preventDefault();   } },false) });
document.addEventListener('DOMContentLoaded', function () { document.getElementById("lastname").addEventListener('keydown', function(e){ if(e.keyCode === 13 ){ e.preventDefault();  } },false) });
document.addEventListener('DOMContentLoaded', function () { document.getElementById("remove").addEventListener('click', function(){ disconnectFromRiff(); },false) });


function getConnectorDescription(label) {
    for(var i =0;i<connectors.length;i++) {
        if(connectors[i].label==label) {
            return connectors[i].description;
        }
    }
}

function setConnectorOff(player) 
{
        var disabledArray = JSON.parse(localStorage["disabledConnectors"]);
        if(disabledArray===null) {
            disabledArray=   localStorage.disabledConnectors = JSON.stringify([]);
            localStorage["disabledConnectors"]=disabledArray

        }
        if(!isConnectorOff(player)) {
            disabledArray.push(player);
            localStorage["disabledConnectors"]=JSON.stringify(disabledArray);
            console.log(localStorage["disabledConnectors"]);
        }
}

function setConnectorOn(player) {
        var disabledArray = JSON.parse(localStorage["disabledConnectors"]);
        if(disabledArray===null) {
            return;
        }
        var i = disabledArray.indexOf(player)
        if(i != -1) {
            disabledArray.splice(i, 1);
            localStorage["disabledConnectors"]=JSON.stringify(disabledArray);
            console.log(localStorage["disabledConnectors"]);
        }
}

function isConnectorOff(player) {
    var disabledArray = JSON.parse(localStorage["disabledConnectors"]);
        var i = disabledArray.indexOf(player)
        return (i!=-1);
}

/**
 * Swap between full colour and grey depending on on/off switch.
 * @param player
 */
function changeImage(player) 
{   
    console.log("IMAGE CHANGED");
    console.log(player);

    var iconNot = null;
        if  (document.getElementById(player).checked == false)
        {
                 setConnectorOff(player); 
                 var message = "Scrobbling to Riff OFF";
        }
        else 
        {       
                setConnectorOn(player);     
                var message = "Scrobbling to Riff ON";      
        }  
        /**
         * Generate the bottom right popup.
         */
        iconNot = "https://riff-resource.s3.amazonaws.com/services/"+player+".png";
        var notification = webkitNotifications.createNotification( iconNot,message, getConnectorDescription(player));
        notification.show();
        setTimeout(function() {notification.cancel()}, 800);              
}
        
/**
 * Store a player with a switch that is turned off.
 * @returns
 */
function restore_options_s() {
    for (var i = 0; i<supportedPlayers.length; i++)
    {   
        var player = supportedPlayers[i];
            if (isConnectorOff(player)) 
            {           
                player = "#"+ player;
                $(player).prop('checked', false);
                console.log ("player off");      
            }
            else
            {
                player = "#"+ player;
                $(player).prop('checked', true);
                console.log ("player off");             
            }
    }
}

/**
 * Handle verifying a Riff Session.
 * @returns
 */
function connectToRiff() {
    var code = document.getElementById("lastname").value
    if(code) {
        code = code.toUpperCase();
    }
    var riff = new Riff();

    riff.verify(code, function(accessToken) {
        localStorage["session"] = accessToken;
        
        if (localStorage["session"]!=""){
            swap("whenLoggedIn", "whenLoggedOut");
            /**
             * Sure to cancel this in case it was previously set.
             */
            document.getElementById("con-error").style.display = "none";
            }
        else {
            document.getElementById("con-error").style.display = "block";
        }
    });
}

/**
 * Break the session link.
 * @returns
 */
function disconnectFromRiff() {
    console.log("disconnected"); 
    var riff = new Riff();
    riff.logout(function() {
        document.getElementById("lastname").value="";
        swap("whenLoggedOut", "whenLoggedIn"); 
    });
}

/**
 * Swapping between DIV's depending on login status.
 */
function swap(one, two) {
        document.getElementById(one).style.display = 'block';
        document.getElementById(two).style.display = 'none';
        
        if (one == "whenLoggedOut")
        {
        document.getElementById("getapp").style.display = 'block';
            document.getElementById("logout").style.display = 'none';
          document.getElementById("song-meta-container").style.display = 'none';

        document.getElementById("headerLoggedOut").style.display = 'block';
            document.getElementById("headerLoggedIn").style.display = 'none';       
                    
        }
        
        else
                {
            document.getElementById("getapp").style.display = 'none';
            document.getElementById("logout").style.display = 'block';
                  document.getElementById("song-meta-container").style.display = 'block';
            
        document.getElementById("headerLoggedOut").style.display = 'none';
            document.getElementById("headerLoggedIn").style.display = 'block'; 
        }
        
}  



    
    
        