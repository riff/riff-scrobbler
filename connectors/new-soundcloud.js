var SC_KEY = "9f353e330fd556de1281c7f25e6c331c";

/**
 * Created by dennis on 6/24/15.
 * Replaces old soundcloud connector, which stopped working.
 *
 * This one tries to hit soundcloud to get all the details of the playing song, falling back to the displayed song title
 * if the request to get details fails.
 */
$(function () {
  // On page load, grab metadata.
  var $title = $('.playbackSoundBadge__title');
  var metadata = $title.attr('title'); // Imagine Dragons - Shots
  var url = $title.attr('href'); // /broilerofficial/shots    (permalink)
  if (metadata) {
    console.log(metadata);
    updateNowPlaying(metadata, url);
  }

  // Query the title every 500ms to check for changes.
  // updateNowPlaying handles ignoring duplicates! meaning this timer won't override any other open tabs (tested)
  var scInterval = setInterval(function() {
    // please don't console.log here
    var $title = $('.playbackSoundBadge__title');
    var metadata = $title.attr('title'); // Imagine Dragons - Shots
    var url = $title.attr('href'); // /broilerofficial/shots    (permalink)

    updateNowPlaying(metadata, url);
  }, 500);

  // Mark 1 of a coder that is feeling good about himself: clears javascript intervals
  // To be honest, I have no idea if $(window).unload ever fires, I just copied this from somewhere in the codebase
  $(window).unload(function () {
    clearInterval(scInterval);

    //reset the background scrobbler song data
    chrome.runtime.sendMessage({
      type: 'reset'
    });
    return true;
  });
});

/**
 * Parse given string into artist and track, assume common order Art - Ttl
 * @return {artist, track}
 */
function parseInfo(artistTitle) {
  var artist = '';
  var track = '';

  var separator = findSeparator(artistTitle);
  if (separator == null)
    return {
      artist: '',
      track: artistTitle
    };

  artist = artistTitle.substr(0, separator.index);
  track = artistTitle.substr(separator.index + separator.length);

  return cleanArtistTrack(artist, track);
}

/**
 * Find first occurence of possible separator in given string
 * and return separator's position and size in chars or null.
 */
function findSeparator(str) {
  // care - minus vs hyphen.
  var separators = [' - ', ' – ', '-', '–', ':'];

  // check the string for match.
  for (i in separators) {
    var sep = separators[i];
    var index = str.indexOf(sep);
    if (index > -1)
      return {
        index: index,
        length: sep.length
      };
  }

  return null;
}

/**
 * Clean non-informative garbage from title
 */
function cleanArtistTrack(artist, track) {
  // Do some cleanup
  artist = artist.replace(/^\s+|\s+$/g, '');
  track = track.replace(/^\s+|\s+$/g, '');

  // Strip crap
  track = track.replace(/^\d+\.\s*/, ''); // 01.
  track = track.replace(/\s*\*+\s?\S+\s?\*+$/, ''); // **NEW**
  track = track.replace(/\s*\[[^\]]+\]$/, ''); // [whatever]
  track = track.replace(/\s*\([^\)]*version\)$/i, ''); // (whatever version)
  track = track.replace(/\s*\.(avi|wmv|mpg|mpeg|flv)$/i, ''); // video extensions
  track = track.replace(/\s*(of+icial\s*)?(music\s*)?video/i, ''); // (official)? (music)? video
  track = track.replace(/\s*\(\s*of+icial\s*\)/i, ''); // (official)
  track = track.replace(/\s*\(\s*[0-9]{4}\s*\)/i, ''); // (1999)
  track = track.replace(/\s+\(\s*(HD|HQ)\s*\)$/, ''); // HD (HQ)
  track = track.replace(/\s+(HD|HQ)\s*$/, ''); // HD (HQ)
  track = track.replace(/\s*video\s*clip/i, ''); // video clip
  track = track.replace(/\s+\(?live\)?$/i, ''); // live
  track = track.replace(/\(\s*\)/, ''); // Leftovers after e.g. (official video)
  track = track.replace(/^(|.*\s)"(.*)"(\s.*|)$/, '$2'); // Artist - The new "Track title" featuring someone
  track = track.replace(/^(|.*\s)'(.*)'(\s.*|)$/, '$2'); // 'Track title'
  track = track.replace(/^[\/\s,:;~-]+/, ''); // trim starting white chars and dash
  track = track.replace(/[\/\s,:;~-]+$/, ''); // trim trailing white chars and dash

  return {
    artist: artist,
    track: track
  };
}

/**
 * Clean the metadata.
 */
var cleanMetadata = function(metadata) {
  // Sometimes the artist name is in the track title.
  // e.g. Tokyo Rose - Zender Overdrive by Aphasia Records.
  var data = parseInfo(metadata);
  if (!data.artist) data.artist = '';

  // return clean metadata object.
  return data;
};

var current = {
  metadata: null,
  validated: null,
  lastMetadataChange: 0
};

// Anytime you suspect there may be any chance of the song changing, call this.
function updateNowPlaying(metadata, url) {
  // Handle duplicates.
  if (metadata === current.metadata) return;
  if (new Date() - current.lastMetadataChange < 1000) return; // only process 1 song change per second, since we run a request each change
  current.metadata = metadata;
  current.lastMetadataChange = new Date();

  // Look it up on soundcloud api.
  var fullScPermalink = "http://soundcloud.com" + url;
  var scResolveUrl = "https://api.soundcloud.com/resolve.json?client_id=" + SC_KEY + "&url=" + encodeURIComponent(fullScPermalink);

  chrome.runtime.sendMessage({ type: "xhr", url: scResolveUrl }, function(response) {
    // Prepare data.
    var data;
    console.log(response);
    if (response.success) {
      var rawData = JSON.parse(response.text);

      data = cleanMetadata(rawData.title);
      if (!data.artist) {
        data.artist = rawData.user.username;
      }

      data.duration = rawData.duration / 1000; // ms -> s
      data.songId = rawData.id;
      data.source = 'soundcloud';
    } else {
      data = cleanMetadata(metadata);
    }

    console.log(data);

    // Initialize connection
    chrome.runtime.sendMessage({
      type: 'reset'
    });
    // Send information.
    chrome.runtime.sendMessage({
          type: 'validate',
          artist: data.artist,
          track: data.track
        },
        function(response) {
          current.validated = response;
          console.log('response', response);
          if (response !== false) {
            chrome.runtime.sendMessage({
              type: 'nowPlaying',
              artist: response.artist,
              track: response.track,
              duration: data.duration,
              source: data.source,
              songId: data.songId
            });
          }
        });
  });
}
