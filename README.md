# Riff Music Capture (Chrome)

Scrobbles your songs from desktop Chrome to the Recent Songs list on your Riff app.

Get it at https://chrome.google.com/webstore/detail/riff-music-capture/daaaenfdfejokiifimmpajbhgabkimmb, and get Riff at http://www.riff-app.com!

Based on https://github.com/david-sabata/web-scrobbler and the Soundwave scrobbler (GPLv3)
