function Riff() {
  this.verify = function (code, callback) {
    localStorage["access_token"] = "";

    $.post(RIFF_VERIFY_ENDPOINT,
        {session: {authentication_code: code}},
        function success(data) {
          console.log('Authenticate response: %s', data);

          if (!data || !data.success) {
            console.log("Authenticate request failed.");
            return "";
          }

          localStorage["access_token"] = data.access_token;

          callback(data.access_token);
        }, 'json'
    );
  }

  this.logout = function (callback) {
    var token = localStorage["access_token"];
    if (token == null) {
      console.log("Not going to log out. No session");
      return false;
    }

    return this.executePost(RIFF_LOGOUT_ENDPOINT, token, {},
        function onLogOut() {
          localStorage["session"] = "";

          callback();
        },
        function onRejectLogOut() {
          console.log("deauthenticate rejected...");
          // I mean... if you couldnt log out... you're probably not logged in...?
          localStorage["session"] = "";

          callback();
        }
    );
  }

  this.notificationId = 20;

  this.scrobble = function (push, callback) {
    var token = localStorage["access_token"];
    if (token == null) {
      console.log("Can't start scrobble, no auth token");
      return false;
    }

    if (DEBUG_MODE) {
      console.log(push);
      var scrobbleNotificationId = "" + this.notificationId;
      this.notificationId += 1;
      console.log(scrobbleNotificationId);
      chrome.notifications.create(scrobbleNotificationId, {
            type: "basic",
            iconUrl: "icon128.png",
            title: push.scrobble.song_title,
            message: push.scrobble.song_artist,
            priority: 2,
            buttons: [{title: "Dismiss"}],
            isClickable: false
          },
          function (notificationId) {
          }
      );
      return true;
    } else return this.executePost(RIFF_SCROBBLE_ENDPOINT, token, push, callback);
  }

  this.token = function () {
    return localStorage["access_token"];
  }

  this.isAuthenticated = function () {
    return !!this.token();
  }

  this.buildScrobbleSingle = function (title, artist, album, source) {
    var song = {
      title: title,
      artist: artist,
      album: album,
      source: source
    };

    return {
      scrobble: this.buildScrobble(song)
    };
  };

  this.buildScrobble = function (song) {
    var scrobble = {
      song_title: song.title,
      song_artist: song.artist || '',
      song_album: song.album,
      device: "computer",
      platform: "chrome",
      external_source: song.source,
      external_song_id: song.songId
    };

    return scrobble;
  };

  this.executePost = function (endpoint, token, body, successCallback, errorCallback) {
    var that = this;
    var oldArguments = arguments;

    if (!successCallback) {
      successCallback = function (response, status, jqXHR) {
        console.log('POSTed %s with status %s', endpoint, status);
      };
    }
    if (!errorCallback) {
      errorCallback = function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 529) {
          // If it's a rate limit, just retry it in a bit
          console.log("Rate limited -- retrying in 300ms");
          setTimeout(function () {
            that.executePost.apply(that, oldArguments);
          }, 300);
        } else {
          console.log('Error POSTing %s; jQuery says %s, server says %s',
              endpoint, textStatus, errorThrown);
        }
      };
    }

    if (!body) {
      body = {};
    }
    body.access_token = this.token();

    $.ajax({
      type: 'POST',
      url: endpoint,
      data: body,
      success: successCallback,
      error: errorCallback,
      dataType: 'json'
    });

    return true;
  }
}